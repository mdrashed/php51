<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>GPA Calculation</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<?php if(isset($_SESSION['error']) && !empty($_SESSION['error'])){?>
		<div class="alert alert-danger"><?=$_SESSION['error']; ?></div>
<?php unset($_SESSION['error']); } ?>

<form action="gradepoint.php" method="post">
  <div class="input-group">
    <span class="form-level">Bangla</span>
    <input type="text" class="form-control" required="" name="bangla" placeholder="Bangla">
  </div>
  <div class="input-group">
    <span class="form-level">Math</span>
    <input type="text" class="form-control" required="" name="math" placeholder="Math">
  </div>
  <div class="input-group">
    <span class="form-level">English</span>
    <input type="text" class="form-control" required="" name="english" placeholder="English">
  </div>
  <div class="input-group">
    <span class="form-level">Physics</span>
    <input type="text" class="form-control" required="" name="physics" placeholder="Physics">
  </div>
  <div class="input-group">
    <input type="submit" class="btn btn-primary" name="submit" value="Submit">
  </div>
</form>
</div>
</body>
</html>