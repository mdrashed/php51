<?php session_start();

class gradepoint
{
   public $Bangla, $Math, $English ,$Physics, $output, $number, $points, $gpa;

	public function point($number){
		switch($number){
		   	case(($number >=90) && ($number <=100)):
		   		$this->points = "Golden A+";
		   		break;
		   	case(($number >=80) && ($number <=89)):
		   		$this->points = "A+";
		   		break;
		   	case(($number >=70) && ($number <=79)):
		   		$this->points = "A";
		   		break;
		   	case(($number < 70) && ($number > 1)):
		   		$this->points = "Fail";
		   		break;
		}
		return $this->points;
	}
	public function setMarks($input1,$input2,$input3,$input4)
   {
		$this->Bangla =  $this->point($input1);
		$this->Math =  $this->point($input2);
		$this->English =  $this->point($input3);
		$this->Physics =  $this->point($input4);
		
		$diff = array($this->Bangla,$this->Math,$this->English,$this->Physics);

		if(($this->Bangla == $this->Math) && ($this->English == $this->Physics)){
			return $this->Bangla;
		}elseif(($this->Bangla == "Fail")|| ($this->Math == "Fail") || ($this->English == "Fail") || ($this->Physics == "Fail")){
			return "Fail";
		}else{
   			return $this->point(($input1*$input2*$input3*$input4)/4);
   		}
	}
}
$obj = new gradepoint();

if(isset($_POST['submit']) && !empty($_POST['bangla']) && !empty($_POST['math']) && !empty($_POST['english']) && !empty($_POST['physics'])){
	if((($_POST['bangla'] < 1) || ($_POST['bangla'] > 100)) || (($_POST['math'] < 1) || ($_POST['math'] > 100)) || (($_POST['english'] < 1) || ($_POST['english'] > 100)) || (($_POST['physics'] < 1) || ($_POST['physics'] > 100))){
		$_SESSION['error'] = "Out Of Range. Please input valid Number.";
		header("location:gradepiont_form.php");
	}else{
		echo $res = $obj->setMarks($_POST['bangla'],$_POST['math'],$_POST['english'],$_POST['physics']);
	}
}else{
	$_SESSION['error'] = "Please Fillup all field."; 
	header("location:gradepiont_form.php");
}