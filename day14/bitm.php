<?php
?>
<html>
<head>

</head>
<body>

<!-- Body section-->
<div class="body_wrapper">
    <div class="event_section">

        <div class="middle_event">
            <div class="professional" style="width:730px;">
                <h2 class="event_title">Registration for SEIP Project</h2>

                <div class="seip-form">
                    <form action="" method="post" id="seipRegistrationForm" class="infoForm" enctype="multipart/form-data">
                        <div class="input_holder-2 margin-right" style="width: 350px;">
                            <label>Select Course:(One person will be eligible for one course only)</label>
                            <span style="width: 120px; display: block; float: left;">First Preference<span style="color:red;">*</span>:</span>
                            <select name="fp_cource_id" id="fp_cource_id" class="input-text" style="width:64%" onchange="setPreferValue(this.value);">
                                <option value="">Select Course</option>
                                <option value="1"  >Affiliate Marketing</option>
                                <option value="2"  >Web App Develop- PHP</option>
                                <option value="3"  >Digital Marketing Course</option>
                                <option value="4"  >Mobile App Develop</option>
                                <option value="5"  >Graphics & Web UI design</option>
                                <option value="6"  >Web design</option>
                                <option value="7"  >Practical SEO</option>
                                <option value="8"  >Web App Develop-Dot Net</option>
                                <option value="9"  >Customer Support & Service</option>
                                <option value="10"  >IT Sales</option>
                                <option value="11"  >IT support Technical</option>
                                <option value="12"  >Server Administration & Cloud Management</option>
                                <option value="13"  >English & business Communication</option>
                            </select>
                            <span style="width: 120px; display: block; float: left;">Second Preference<span style="color:red;">*</span>:</span>
                            <select name="sp_cource_id" id="sp_cource_id" class="input-text" style="width:64%">
                                <option value="">Select Course</option>
                                <option value="1"  >Affiliate Marketing</option>
                                <option value="2"  >Web App Develop- PHP</option>
                                <option value="3"  >Digital Marketing Course</option>
                                <option value="4"  >Mobile App Develop</option>
                                <option value="5"  >Graphics & Web UI design</option>
                                <option value="6"  >Web design</option>
                                <option value="7"  >Practical SEO</option>
                                <option value="8"  >Web App Develop-Dot Net</option>
                                <option value="9"  >Customer Support & Service</option>
                                <option value="10"  >IT Sales</option>
                                <option value="11"  >IT support Technical</option>
                                <option value="12"  >Server Administration & Cloud Management</option>
                                <option value="13"  >English & business Communication</option>
                            </select>
                            <span style="width: 120px; display: block; float: left;">Third Preference<span style="color:red;">*</span>:</span>
                            <select name="tp_cource_id" id="tp_cource_id" class="input-text" style="width:64%">
                                <option value="">Select Course</option>
                                <option value="1"  >Affiliate Marketing</option>
                                <option value="2"  >Web App Develop- PHP</option>
                                <option value="3"  >Digital Marketing Course</option>
                                <option value="4"  >Mobile App Develop</option>
                                <option value="5"  >Graphics & Web UI design</option>
                                <option value="6"  >Web design</option>
                                <option value="7"  >Practical SEO</option>
                                <option value="8"  >Web App Develop-Dot Net</option>
                                <option value="9"  >Customer Support & Service</option>
                                <option value="10"  >IT Sales</option>
                                <option value="11"  >IT support Technical</option>
                                <option value="12"  >Server Administration & Cloud Management</option>
                                <option value="13"  >English & business Communication</option>
                            </select>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-2 margin-right">
                                <label>Name:<span>*</span></label>
                                <input type="text" class="input-text" name="name" id="name" value="" />
                            </div>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Father Name:<span>*</span></label>
                                <input type="text" class="input-text" name="fname" id="fname" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Mother Name:<span>*</span></label>
                                <input type="text" class="input-text" name="mnane" id="mnane" value="" />
                            </div>
                            <div class="input_holder-3 margin-right-0" style=" width: 202px;">
                                <label>Gender:<span>*</span></label>
                                <select name="gender" id="gender" class="input-text">
                                    <option value="">Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>

                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>National ID:</label>
                                <input type="text" class="input-text" name="NID" id="NID" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Birth Certificate No:</label>
                                <input type="text" class="input-text" name="BCN" id="BCN" value="" />
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Other Valid Id (e.g passport):</label>
                                <input type="text" class="input-text" name="OID" id="OID" value="" />
                            </div>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Date of Birth:<span>*</span></label>
                                <input type="text" class="input-text datepicker" name="DOB" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Religion:<span>*</span></label>
                                <select name="religion" id="religion" class="input-text">
                                    <option value="">Select Religion</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Hinduism">Hinduism</option>
                                    <option value="Buddhism">Buddhism</option>
                                    <option value="Christianity">Christianity</option>
                                </select>
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Ethnic Group:<span>*</span></label>
                                <select name="ethnic_group" id="ethnic_group" class="input-text">
                                    <option value="">Select Ethnic Group</option>
                                    <option value="Armenian community of Dhaka">Armenian community of Dhaka</option>
                                    <option value="Armenians in Bangladesh">Armenians in Bangladesh</option>
                                    <option value="Bagdi caste">Bagdi caste</option>
                                    <option value="Bom people">Bom people</option>
                                    <option value="Bede people">Bede people</option>
                                    <option value="Bedia (Muslim caste)">Bedia (Muslim caste)</option>
                                    <option value="Bengali Hindus">Bengali Hindus</option>
                                    <option value="Bengali Muslims">Bengali Muslims</option>
                                    <option value="Bengali people">Bengali people</option>
                                    <option value="Bihari Muslims">Bihari Muslims</option>
                                    <option value="Bihari people">Bihari people</option>
                                    <option value="Buno people">Buno people</option>
                                    <option value="Chak people">Chak people</option>
                                    <option value="Chakma people">Chakma people</option>
                                    <option value="Chinese people in Bangladesh">Chinese people in Bangladesh</option>
                                    <option value="Dhanuk">Dhanuk</option>
                                    <option value="Garo people">Garo people</option>
                                    <option value="Indians in Bangladesh">Indians in Bangladesh</option>
                                    <option value="Indigenous peoples in Bangladesh">Indigenous peoples in Bangladesh</option>
                                    <option value="Jumma people">Jumma people</option>
                                    <option value="Kan (tribe)">Kan (tribe)</option>
                                    <option value="Khasi people">Khasi people</option>
                                    <option value="Khumi people">Khumi people</option>
                                    <option value="Khyang people">Khyang people</option>
                                    <option value="Kuki Inpi">Kuki Inpi</option>
                                    <option value="Kuki people">Kuki people</option>
                                    <option value="Kurukh people">Kurukh people</option>
                                    <option value="Lushei tribe">Lushei tribe</option>
                                    <option value="Mahle people">Mahle people</option>
                                    <option value="Maimal">Maimal</option>
                                    <option value="Mustafa Majid">Mustafa Majid</option>
                                    <option value="Mal Muslim">Mal Muslim</option>
                                    <option value="Marma people">Marma people</option>
                                    <option value="Mro people">Mro people</option>
                                    <option value="Munda people">Munda people</option>
                                    <option value="Murang people">Murang people</option>
                                    <option value="Naulak">Naulak</option>
                                    <option value="Nepalis in Bangladesh">Nepalis in Bangladesh</option>
                                    <option value="Pankho people">Pankho people</option>
                                    <option value="Rakhine people, Bangladesh">Rakhine people, Bangladesh</option>
                                    <option value="Rohingya people">Rohingya people</option>
                                    <option value="Santhal people">Santhal people</option>
                                    <option value="Stranded Pakistanis">Stranded Pakistanis</option>
                                    <option value="Tanchangya people">Tanchangya people</option>
                                    <option value="Thadou people">Thadou people</option>
                                    <option value="Template:Tribes of Bangladesh">Template:Tribes of Bangladesh</option>
                                    <option value="Tripuri people">Tripuri people</option>
                                    <option value="Vaiphei people">Vaiphei people</option>
                                    <option value="Zo people">Zo people</option>
                                    <option value="Zomi nationalism">Zomi nationalism</option>
                                </select>
                            </div>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-2 margin-right">
                                <label>Mobile:<span>*</span></label>
                                <input type="text" class="input-text" name="mobile" id="mobile" value="" />
                            </div>
                            <div class="input_holder-2 margin-right-0">
                                <label>Email:<span>*</span></label>
                                <input type="text" class="input-text" name="email" id="email" value="" />
                            </div>
                        </div>

                        <div class="employed-block">
                            <div class="employed-left">
                                <label>Is Employed?</label>
                                <input type="radio" name="employee" value="yes"  onclick="show_company_info('yes')">Yes
                                <input type="radio" name="employee" value="no" onclick="show_company_info('no')">No
                            </div>
                            <div class="employed-right">
                                <label>Monthly Income:</label>
                                <input type="text" class="input-text" name="mincome" id="mincome" value="" />
                            </div>
                            <div class="input_holder" id="company_info">
                                <div class="input_holder-2 margin-right">
                                    <label>Company Name:<span>*</span></label>
                                    <input type="text" class="input-text" name="company_name"  value="" />
                                </div>
                                <div class="input_holder-2 margin-right-0">
                                    <label>Designation:<span>*</span></label>
                                    <input type="text" class="input-text" name="designation"  value="" />
                                </div>
                            </div>
                            <div class="input_holder" id="preferable_framework">
                                <label style="display: inline-block">Preferable Framework : </label>
                                <input type="radio" name="preferable_framework" value="codeigniter"  > Codeigniter
                                <input type="radio" name="preferable_framework" value="laravel" >Laravel
                                <input type="radio" name="preferable_framework" value="php" >Raw PHP
                            </div>
                        </div>

                        <h3>Present Address</h3>
                        <div class="input_holder" style="width: 668px;">
                            <label>Address:<span>*</span></label>
                            <input type="text" class="input-text" name="pre_address" id="pre_address" value="" />
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-2 margin-right">
                                <label>City:<span>*</span></label>
                                <input type="text" class="input-text" name="pre_city" id="pre_city" value="" />
                            </div>
                            <div class="input_holder-2 margin-right-0">
                                <label>Post Code:<span>*</span></label>
                                <input type="text" class="input-text" name="pre_postcode" id="pre_postcode" value="" />
                            </div>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Division:<span>*</span></label>
                                <select name="pre_division" class="input-text" id="pre_division">
                                    <option value="" selected="selected">Select Division</option>
                                    <option value="5">Barisal</option>
                                    <option value="2">Chittagong</option>
                                    <option value="1">Dhaka</option>
                                    <option value="3">Khulna</option>
                                    <option value="6">Rajshahi</option>
                                    <option value="7">Rangpur</option>
                                    <option value="4">Sylhet</option>
                                </select>
                            </div>
                            <div class="input_holder-3">
                                <label>District:<span>*</span></label>
                                <select  name="pre_district" id="pre_district" class="input-text">
                                    <option value="0" selected="selected">Select District</option>
                                </select>


                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Sub-District:<span>*</span></label>
                                <select  name="pre_sub_district" id="pre_sub_district" class="input-text">
                                    <option value="0" selected="selected">Select Sub-District</option>
                                </select>
                            </div>
                        </div>
                        <h3>Permanent Address</h3>
                        <div class="input_holder" style="width: 668px;">
                            <label>Address:<span>*</span></label>
                            <input type="text" class="input-text" name="per_address" id="per_address" value="" />
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-2 margin-right">
                                <label>City:<span>*</span></label>
                                <input type="text" class="input-text" name="per_city" id="per_city" value="" />
                            </div>
                            <div class="input_holder-2 margin-right-0">
                                <label>Post Code:<span>*</span></label>
                                <input type="text" class="input-text" name="per_postcode" id="per_postcode" value="" />
                            </div>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Division:<span>*</span></label>
                                <select name="per_division" class="input-text" id="per_division">
                                    <option value="" selected="selected">Select Division</option>
                                    <option value="5">Barisal</option>
                                    <option value="2">Chittagong</option>
                                    <option value="1">Dhaka</option>
                                    <option value="3">Khulna</option>
                                    <option value="6">Rajshahi</option>
                                    <option value="7">Rangpur</option>
                                    <option value="4">Sylhet</option>
                                </select>
                            </div>
                            <div class="input_holder-3">
                                <label>District:<span>*</span></label>
                                <select  name="per_district" id="per_district" class="input-text">
                                    <option value="0" selected="selected">Select District</option>
                                </select>
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Sub-District:<span>*</span></label>
                                <select  name="per_sub_district" id="per_sub_district" class="input-text">
                                    <option value="0" selected="selected">Select Sub-District</option>
                                </select>
                            </div>
                        </div>
                        <h3>SSC or Equivalent</h3>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Board:<span>*</span></label>
                                <input type="text" class="input-text" name="ssc_board" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Institute:<span>*</span></label>
                                <input type="text" class="input-text" name="ssc_institue" value="" />
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Subject:<span>*</span></label>
                                <input type="text" class="input-text" name="ssc_subject" value="" />
                            </div>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Year:<span>*</span></label>
                                <input type="text" class="input-text" name="ssc_pass_year" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Roll Number:<span>*</span></label>
                                <input type="text" class="input-text" name="ssc_rollno" value="" />
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Result:<span>*</span></label>
                                <input type="text" class="input-text" name="ssc_result" value="" />
                            </div>
                        </div>
                        <h3>HSC or Equivalent</h3>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Board:<span>*</span></label>
                                <input type="text" class="input-text" name="hsc_board" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Institute:<span>*</span></label>
                                <input type="text" class="input-text" name="hsc_institue" value="" />
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Subject:<span>*</span></label>
                                <input type="text" class="input-text" name="hsc_subject" value="" />
                            </div>
                        </div>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Year:<span>*</span></label>
                                <input type="text" class="input-text" name="hsc_pass_year" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Roll Number:<span>*</span></label>
                                <input type="text" class="input-text" name="hsc_rollno" value="" />
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Result:<span>*</span></label>
                                <input type="text" class="input-text" name="hsc_result" value="" />
                            </div>
                        </div>
                        <h3>Highest Academic Achievement</h3>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Level of Education:</label>
                                <input type="text" class="input-text" name="education" id="education" value="" />
                            </div>
                            <div class="input_holder-3">
                                <label>Institute:</label>
                                <input type="text" class="input-text" name="hinstitue" id="hinstitue" value="" />
                            </div>
                            <div class="input_holder-3 margin-right-0">
                                <label>Subject:</label>
                                <input type="text" class="input-text" name="hsubject" id="hsubject" value="" />
                            </div>
                        </div>
                        <h3>Photo</h3>
                        <div class="input_holder">
                            <div class="input_holder">
                                <input type="file" name="photo" /><br>
                                <span class="help-block" style="color: #888889">Image size should be 300x300 (Max 100KB) in JPG Format </span>
                            </div>
                        </div>
                        <h3>Security</h3>
                        <div class="input_holder">
                            <div class="input_holder-3">
                                <label>Type the code:<span>*</span></label>
                                <input type="text" class="input-text" name="security_code" id="security_code" value="" />
                            </div>
                            <div class="input_holder-3">

                                <div id="capImg" style="float: left; margin-top: 18px;"><img src="https://bitm.org.bd/captcha/1491545450.8612.jpg" width="120" height="35" style="border:0;" alt=" " align="absmiddle" /></div>
                                <a href="javascript:void(0)" id="new_captcha" style="float: left; margin-top: 25px;">
                                    <img src="https://bitm.org.bd/assets/images/refresh.gif" alt="Reload" title="Reload new code" />
                                </a>
                            </div>

                        </div>
                        <div class="input_holder">

                        </div>

                        <div style=" text-align: center">
                            <input class="form_btn form_btn_reset" name="submit" type="reset" value="Reset">
                            <input class="form_btn" name="submit" type="submit" value="Save">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="right_event">
            <h2 class="event_title">NEWS</h2>
            <div class="news_event">
                <div class="news_event_img"><img src="https://bitm.org.bd/uploads/1425556639.jpg"></div>                        <div class="news_event_box">
                    <h3>
                        <a href="https://bitm.org.bd/basis-will-create-23000-it-skilled-manpower-under-skill-for-employment-investment-program-seip-a-project-of-the-ministry-of-finance/n183">
                            BASIS will Create 23000 IT Skilled Manpower under Skill for Employment Investment Program (SEIP), a project of the Ministry of Finance                                </a>
                    </h3>
                    <span>05 Mar 2015</span>
                    <div class="details">
                        <a href="https://bitm.org.bd/basis-will-create-23000-it-skilled-manpower-under-skill-for-employment-investment-program-seip-a-project-of-the-ministry-of-finance/n183">Details</a>
                    </div>
                </div>
            </div>
            <div class="news_event">
                <div class="news_event_img"><img src="https://bitm.org.bd/uploads/1424241677.jpg"></div>                        <div class="news_event_box">
                    <h3>
                        <a href="https://bitm.org.bd/code-warriors-challenge-2015-at-award-night-of-digital-world-2015/n178">
                            Code Warriors Challenge 2015 at Award Night of Digital World 2015.                                </a>
                    </h3>
                    <span>12 Feb 2015</span>
                    <div class="details">
                        <a href="https://bitm.org.bd/code-warriors-challenge-2015-at-award-night-of-digital-world-2015/n178">Details</a>
                    </div>
                </div>
            </div>
            <div class="news_event">
                <div class="news_event_img"><img src="https://bitm.org.bd/uploads/1424239805.jpg"></div>                        <div class="news_event_box">
                    <h3>
                        <a href="https://bitm.org.bd/code-warrior-challenge-2015-boot-camp-36-hours/n177">
                            Code Warrior Challenge 2015 (Boot Camp-36 Hours)                                </a>
                    </h3>
                    <span>30 Jan 2015</span>
                    <div class="details">
                        <a href="https://bitm.org.bd/code-warrior-challenge-2015-boot-camp-36-hours/n177">Details</a>
                    </div>
                </div>
            </div>
            <div class="news_event">
                <div class="news_event_img"><img src="https://bitm.org.bd/uploads/1421924123.jpg"></div>                        <div class="news_event_box">
                    <h3>
                        <a href="https://bitm.org.bd/tech-adda-with-shishir-the-first-bd-employee-at-google/n167">
                            Tech Adda with Shishir, the First BD Employee at Google                                </a>
                    </h3>
                    <span>21 Jan 2015</span>
                    <div class="details">
                        <a href="https://bitm.org.bd/tech-adda-with-shishir-the-first-bd-employee-at-google/n167">Details</a>
                    </div>
                </div>
            </div>
            <div class="news_event">
                <div class="news_event_img"><img src="https://bitm.org.bd/uploads/1420878438.jpg"></div>                        <div class="news_event_box">
                    <h3>
                        <a href="https://bitm.org.bd/code-warrior-challenge-2015-grooming-session-1/n163">
                            Code Warrior Challenge 2015 (Grooming Session-1)                                </a>
                    </h3>
                    <span>09 Jan 2015</span>
                    <div class="details">
                        <a href="https://bitm.org.bd/code-warrior-challenge-2015-grooming-session-1/n163">Details</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!-- End Body Section -->
</body>
</html>
