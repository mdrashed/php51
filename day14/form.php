<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <section>
        <div class="header">
            <h1 align="center">Admission Form</h1>
        </div>
        <div class="form">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Admission Related Information</legend>
                            <form action="action.php" method="post">
                                First Name :<span style="color: red">*</span>  <input type="text" name="firstname"><br/><br/>
                              Last Name :<span style="color: red">*</span>   <input type="text" name="lastname"><br/><br/>
                              UserName :<span style="color: red">*</span>   <input type="text" name="username"><br/><br/>
                              E-mail :<span style="color: red">*</span>   <input type="email" name="email" placeholder="example@ex.com"><br/><br>
                              Phone Number :<span style="color: red">*</span>   <input type="number" name="number"><br/><br/>
                              Password :<span style="color: red">*</span>   <input type="password" name="password"><br/><br/>
                              Confirm Password :<span style="color: red">*</span>   <input type="password" name="repassword"><br/><br/>
                              Date : <input type="date" name="date"><br/><br/>
                              Time :  <input type="time" name="time"><br/><br/>
<!--                              Date & Time :  <input type="datetime" name="datetime"><br/><br/>-->

                              Web Address :  <input type="text" name="web"><br/><br/>
<!--                              Select a Color :  <input type="color" name="color"><br/><br/>-->
<!--                              Search anything :  <input type="search" name="search"><br/><br/>-->
                               <div class="skills">
                                   <label>Select your skills :</label><br/>
                                        <input type="checkbox" name="php">PHP<br/><br/>
                                       <input type="checkbox" name="html">HTML <br/><br/>
                                        <input type="checkbox" name="css"> CSS<br/><br/>
                               </div>

                                   <div class="select">
                                    <label>Select item</label>
                                    <select name="selector" >

                                        <option value="item1">item1</option>
                                        <option value="item2">item2</option>
                                        <option value="item3">item3</option>
                                        <option value="item4">item4</option>
                                    </select>
                                   </div>

                                <div class="selectgroup"><br/><br/>
                                    <label>Group select</label>
                                    <select name="selector2">

                                        <optgroup label="GropuSelect">
                                            <option value="item1">item1</option>
                                            <option value="item2">item2</option>
                                            <option value="item3">item3</option>
                                            <option value="item4">item4</option>
                                        </optgroup>
                                        <optgroup label="GroupSelect2">
                                            <option value="item5">item1</option>
                                            <option value="item6">item2</option>
                                            <option value="item7">item3</option>
                                            <option value="item8">item4</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="radiobutton"><br/><br/>
                                    <label>Select Gender:<span style="color: red">*</span>  </label><br/>
                                    <input type="radio" name="gender"value="male" checked>Male<br/>
                                    <input type="radio" name="gender"value="female" >Female<br/>
                                    <input type="radio" name="gender"value="other" >other<br/>

                                </div><br/><br/>
                               <div class="file">
                                   <label>Upload your file</label><br/>
                                   <input type="file" name="file"><br/><br/>
                               </div>



                                  <input type="submit" name="submit" value="Submit">
                                <input type="reset" name="reset"  value="Reset"><br/><br/>
                            </form>
                        </fieldset>
                    </td>
                </tr>
        </div>
    </section>
</body>
</html>