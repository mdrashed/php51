<?php

//assignment operator(+=, -=, *=, /=, %=)

//addition(+=)
$value = 50;
$value += 25;
echo $value;

//subtraction (-=)

$value = 40;
$value -= 5;
    echo "<br>";
    echo $value;

//multiplication (*=)

$a = 15;
$a *= 3;
    echo "<br>";
    echo $a;

//Division (/=)

$a = 50;
$a /= 5;
    echo "<br>";
    echo $a;

//Modulus(%=)

$a = 15;
$a %= 4;
    echo $a;

