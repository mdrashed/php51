<?php


class Calculator
{
    public $number1, $number2, $output;

    public function setNumber($input1 = '', $input2 = '')
    {
        $this->number1 = $input1;
        $this->number2 = $input2;
    }


    public function Calculate()
    {
        if (!empty($_POST['value']) && !empty($_POST['value2'])) {
            if ($_POST['operator'] == '+')
                $this->output = $this->number1 + $this->number2;
            if ($_POST['operator'] == 'sub')
                $this->output = $this->number1 - $this->number2;
            if ($_POST['operator'] == 'mul')
                $this->output = $this->number1 * $this->number2;
            if ($_POST['operator'] == 'div')
                $this->output = $this->number1 / $this->number2;
            if ($_POST['operator'] == 'modulus')
                $this->output = $this->number1 % $this->number2;

        } else {
            echo "Error Number not Provided";
        }
    }

    public function getOutput()
    {
        return $this->output;
    }
}

$Calculator = new Calculator();
$Calculator->setNumber($_POST['value'], $_POST['value2']);
$Calculator->Calculate();
$result = $Calculator->getOutput();
echo $_POST['value'] . $_POST['operator'] . $_POST['value2'] . "=" . $result;
