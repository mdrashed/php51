<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admission Form</title>
</head>

<body>
    <h2 align="center">Admission Form</h2>
    <form action="index.php" method="post">
        <fieldset>
            <legend>Admission Information </legend>

            <!-- SPAN -->
            <label for="fname">First Name: <span>*</span></label>
            <input id="fname" name="fname" type="text" >
            <br> <br>


            <!--autofocus  -->
            <label for="fname">First Name: </label>
            <input id="fname" name="fname" autofocus type="text" >
            <br> <br>

            <!--autocomplete -->
            <label for="fname">First Name: </label>
            <input id="fname" name="fname" autocomplete="off" type="text" >
            <br> <br>

            <!-- required  -->
            <label for="fname">First Name :</label>
            <input id="fname" name="fname" required  type="text" >
            <br><br>

            <!-- maxlength -->
            <label for="fname">First Name :</label>
            <input id="fname" name="fname" maxlength="5"  type="text" >
            <br><br>

            <!-- minlength -->
            <label for="lname" >Last Name :</label>
            <input id="lname" name="lname" minlength="3" type="text" >
            <br><br>

            <!-- VALUE -->
            <label for="fname">First Name : </label>
            <input id="fname" name="Fname" value="example" type="text" >
            <br><br>

            <!-- PLACEHOLDER -->
            <label for="fname">First Name : </label>
            <input id="fname" name="Fname" placeholder="Enter your name" type="text" >
            <br><br>

            <!--  readonly  -->
            <label for="fname">First Name : </label>
            <input id="fname" name="Fname" readonly value=" What's Your destination" type="text" >
            <br><br>

            <!-- Disable -->
            <label for="fname">First Name : </label>
            <input id="fname" name="Fname" disabled type="text" >
            <br><br>

            <!-- RADIO -->
            <input id="Male" name="gender" value="Male" type="radio">
            <label for="Male">male </label>
            <input id="Female" name="gender" value=" Female" type="radio">
            <label for="Female">Female </label>
            <br><br>

            <!-- Password -->
            <label>Password :</label>
            <input type="password"/>
            <br><br>

            <!-- E-mail -->
            <label>E-mail :</label>
            <input type="email"/>
            <br><br>

            <!-- Web Address -->
            <label>Web Address :</label>
            <input type="url"/>
            <br><br>

            <!-- Select a Color -->
            <label>Select a Color :</label>
            <input type="color"/>
            <br><br>

            <!-- Select a Range -->
            <label>Select a Range :</label>
            <input type="range"/>
            <br><br>

            <!-- Date of Birth -->
            <label>Date of Birth :</label>
            <input type="date"/>
            <br><br>

            <!-- Select a Month -->
            <label>Select a Month :</label>
            <input type="month"/>
            <br><br>

            <!-- Select a Week -->
            <label>Select a Week :</label>
            <input type="week"/>
            <br><br>

            <!-- Time -->
            <label>Time :</label>
            <input type="time"/>
            <br><br>

            <!-- Local Time -->
            <label>Local Time :</label>
            <input type="datetime-local"/>
            <br><br>

            <!-- Enter tel -->
            <label>Enter tel number :</label>
            <input type="tel"/>
            <br><br>

            <!-- Enter number -->
            <label>Enter product code :</label>
            <input type="number"/>
            <br><br>

            <!-- Search -->
            <label>Search anything :</label>
            <input type="search"/>
            <br><br>


            <!-- CHECKBOX -->
            <input id="PHP" name="php" value="PHP" type="checkbox">
            <label for="PHP"> PHP </label>
            <input id="HTML" name="html" value="HTML" type="checkbox">
            <label for="HTML"> HTML </label>
            <input id="CSS" name="CSS" value="CSS" type="checkbox">
            <label for="CSS"> CSS </label>
            <br> <br>

            <!-- DROPDOWN OPTION -->
            <label>Select an Item</label>
            <SELECT name='SELECT' >
                <OPTION value='item 1' >Item 1</OPTION>
                <OPTION value='item 2' >Item 2</OPTION>
                <OPTION value='item 3' >Item 3</OPTION>
            </SELECT>
            <br><br>

            <!-- DROPDOWN OPTION with group -->
            <label>Select an Item</label>
            <SELECT name='SELECT' >
                <optgroup label="ITEM">
                    <OPTION value='item 1' >Item 1</OPTION>
                    <optgroup label="ITEM- 02">
                        <OPTION value='item' selected >  Dhaka</OPTION>
                        <OPTION value='item 3' >Gazipur</OPTION>


                    </optgroup>
                </optgroup>

                <optgroup label="GROUP">
                    <OPTION value='group1' >group 1</OPTION>
                    <OPTION value='group2' >group 2</OPTION>
                    <OPTION value='group3' >group 3</OPTION>
                </optgroup>

            </SELECT>

            <br><br>

            <!-- Upload File  -->
            <label>Upload File :</label>
            <input type="file"/>
            <br><br>


            <button type="submit">SUBMIT</button>
            <button type="reset">Reset</button>
        </fieldset>
        <br><br>
        <a href="/dokan.com" target="_blank">go to Home page</a>
    </form>

</body>
</html>


